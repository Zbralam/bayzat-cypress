import '../support/commands'

import Home from '../support/home'

import SignIn from '../support/signin'
import ViewTeam from '../support/viewteam'
//import AddEmployee from '../support/addemployee'


//import SignIn from '../support/signin';
//import SignUp from '../../support/signup';



describe('Main Page Test', function () {
    it('Verify Main Page open Successfully and Login is Clicked', function() {
    
        const home = new Home();
         home.navigate() //Opens website
         
         home.clickLoginButton() // Click Login Button
         
    
    })
    })

describe('Login with Correct Credentials', function(){
    it('Verify that user is able to Login Successfully',function() {
        
        const signin = new SignIn()
        signin.login()
        signin.clickLoginButton()

    })
    })    

describe('Click on View Team, Add Employee, Delete and Logout', function(){
    const viewteam = new ViewTeam()
    it('Verify that user is able to view Team Successfully and Click on Add Employee',function() {
            
            
            viewteam.clickViewTeam();   
            viewteam.clickAddEmployee()
            viewteam.clickAddEmployeeButton()
            viewteam.addEmployeeDetails()
            viewteam.clickViewTeam()
            viewteam.addTextInFilter()
            viewteam.logoutUser()
            

    
        })
    })
      

    







    