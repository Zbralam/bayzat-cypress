import '../support/commands'


class ViewTeam{

clickViewTeam()
{
    cy.wait(5000)
    cy.get('[data-external-id=view-team-link]').click()

}



clickAddEmployee(){
   
    cy.wait(500)
    cy.get('[data-external-id=add-employees-link]').click()
    
}


clickAddEmployeeButton(){

    //cy.get('[href=/enterprise/dashboard/employees/create]').click()
    cy.wait(500);
    cy.get('a[href*="/enterprise/dashboard/employees/create"]').click()
    
}


addEmployeeDetails(){

    var EmailSting = Math.random().toString(36).substr(2, 5)+'zubairalam@gmail.com';
    var nameString = Math.random().toString(36).substr(2, 4);
    cy.get('[name=firstName]').type('Aa'+nameString+'Zubair')
    cy.get('[name=lastName]').type('Alam')
    cy.get('[name=dateOfBirthFormatted]').type('12/05/1995').click()
    cy.get('[name=workEmail]').type(EmailSting)
    //cy.get('#ember426 > .ember-power-select-placeholder').type('Bruneian')
    //cy.get('#ember433 > .ember-power-select-placeholder').type('Male')
    cy.contains('Create').click()
    cy.wait(5000)

    



}

addTextInFilter()
{
    //cy.get('h1').should('have.text', 'Your Tickets');

    cy.get('[type=search]').type('Aa')
    cy.get('div.pad-btm > table > tbody > tr:nth-child(1) > td:nth-child(1)').click()
    cy.get('button.btn.btn-danger.ember-view.mar-rgt--xs.btn-icon').click()
    cy.wait(100)
    cy.get('div > form > button:nth-child(2)').click()

}

logoutUser()
{
     cy.get('[data-external-id=logout-link]').click()

}

















}

export default ViewTeam;
